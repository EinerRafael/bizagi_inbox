# Bizagi Inbox

Permite administrar las solicitudes de vacaciones de tus empleados.

### Run
### Option 1: 
Clone repository  and run this commands
```sh
$ npm install -g local-web-server
$ npm start
```
Open in browser: http://localhost:8100/#/inbox

### Option 2:
    * Clone repository in your htdocs folder
    * Open in browser: http://localhost:apache_port/Bizagi_Inbox
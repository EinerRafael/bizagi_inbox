var Interface = function() {
	
	this.init = function(){
		initializeComponents();
		setEvents();
		return this;
	}

	function initializeComponents () {
		$(document).ready(function(){
			$(".button-collapse").sideNav({
				closeOnClick: true
			});
		});
	}

	function setEvents () {
		$(document).delegate(".reject-request", 'click', function(){
			if (!$(this).hasClass("disabled")) {
				$('#modal_reject_request').openModal();
			};
			return false;
		});
	}

	return this.init();
}
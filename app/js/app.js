var bizagi_app = angular.module("bizagi_app", ["ngRoute", "angularMoment"]);

bizagi_app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/inbox', {
        templateUrl: 'app/templates/inbox.html',
        controller: "InboxController"
      }).
      otherwise({
        redirectTo: '/inbox'
      });
  }]).constant("Config", {
  		"urls":{
	  		"request": "app/data/inbox.json"
	  	}
  })
  .run(function($rootScope){
  	$rootScope.mainTitle = "Inbox";
  	$rootScope.rejectsRequest = [];
  	window.inbox_interface = new Interface();
  });
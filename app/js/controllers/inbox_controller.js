angular.module("bizagi_app").controller("InboxController", ["$scope", "$rootScope", "ConsumerRest", "$window", 
	function ($scope, $rootScope, ConsumerRest, $window) {
	
	$scope.requests = [];
	$scope.selected_request = undefined;

	ConsumerRest.inbox().success(function(json){
		$scope.requests = $window._.sortBy(json.data, "requestDate");
	});

	angular.extend($scope, {
		setSelectedRequest: function(request){
			$scope.selected_request = request;
		},
		approveRequest: function(request){
			$scope.selected_request = request;
			$scope.selected_request.status = 'S';
			$scope.selected_request.processed = true;
		},
		rejectSelectedRequest: function(){
			$scope.selected_request.status = 'N';
			$scope.selected_request.processed = true;
			$scope.selected_request.reject_message = $scope.reason_reject;
			$scope.reason_reject = "";
		}
	});
}]);
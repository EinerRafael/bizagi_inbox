angular.module("bizagi_app").factory('ConsumerRest', ['$http', 'Config',
    function($http, Config) {
    	return {
    		inbox: function(){
    			return $http({
                        url: Config.urls.request,
                        method: "GET"
                    });
    		}
    	}
    }
]);
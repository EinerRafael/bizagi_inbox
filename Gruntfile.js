module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		concat: {
			options: {
				separator: '\r\n',
			},
			bizagi_js: {
				src: [
					"vendor/jquery/dist/jquery.min.js",
					"vendor/underscore/underscore-min.js",
					"vendor/moment/moment.js",
					"vendor/moment/locale/es.js",
					"vendor/angular/angular.min.js",
					"vendor/angular-moment/angular-moment.min.js",
					"vendor/angular-route/angular-route.min.js",
					"vendor/materialize/dist/js/materialize.min.js",
					"app/js/interface.js",
					"app/js/app.js",
					"app/js/factories/consumer_rest.js",
					"app/js/controllers/inbox_controller.js"
				],
				dest: 'dist/js/bizagi_inbox.js'
			}
		},
		uglify: {
			bizagi: {
				files: {
					'dist/js/bizagi_inbox.min.js': ['dist/js/bizagi_inbox.js']
				}
			}
		},
		less: {
			bizagi: {
				files: {
					"dist/css/bizagi_inbox.css": "css/main.less"
				}
			}
		},
		cssmin: {
			bizagi: {
				files: {
					'dist/css/bizagi_inbox.min.css': [
						'vendor/materialize/dist/css/materialize.min.css', 
						'dist/css/bizagi_inbox.css', ]
				}
			}
		},
		copy: {
		  main: {
		  	files: [{
                    expand: true,
                    cwd: 'vendor/materialize/dist/font/',
                    src: ['**'],
                    dest: 'dist/font/'
                }]
		  }
		}
	});

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-copy');

	grunt.registerTask('default', ['concat:bizagi_js', 'uglify:bizagi', 'less:bizagi', 'cssmin:bizagi', 'copy']);
};